package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/kurin/blazer/b2"
	"github.com/mholt/archiver"
)

type config struct {
	BackupLocations,
	ArchiveFile,
	B2Id,
	B2Key,
	B2Bucket string
}

const filePrefix string = "ghost-cron-backup"

func main() {
	cfg := config{
		ArchiveFile:     os.Getenv("ARCHIVE"),
		B2Id:            os.Getenv("B2ID"),
		B2Key:           os.Getenv("B2KEY"),
		B2Bucket:        os.Getenv("B2BUCKET"),
		BackupLocations: os.Getenv("BACKUPLOCATIONS"),
	}

	if err := archive(cfg); err != nil {
		fmt.Println("error while archiving", err)
	}
	if err := uploadToB2(cfg); err != nil {
		fmt.Println("error while uploading to B2", err)
	}
}

func archive(cfg config) error {
	files := strings.Split(cfg.BackupLocations, ",")

	t := archiver.Tar{
		OverwriteExisting:      true,
		MkdirAll:               true,
		ImplicitTopLevelFolder: true,
		ContinueOnError:        false,
	}

	tarXz := archiver.TarXz{
		Tar: &t,
	}

	return tarXz.Archive(files, cfg.ArchiveFile)
}

func uploadToB2(cfg config) error {
	ctx := context.Background()
	f, err := os.Open(cfg.ArchiveFile)
	if err != nil {
		return err
	}
	defer f.Close()
	c, err := b2.NewClient(ctx, cfg.B2Id, cfg.B2Key)
	if err != nil {
		return err
	}
	bucket, err := c.Bucket(ctx, cfg.B2Bucket)
	if err != nil {
		return err
	}
	hostname, err := os.Hostname()
	if err != nil {
		return err
	}
	obj := bucket.Object(filePrefix + "_" + hostname)
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, f); err != nil {
		w.Close()
		return err
	}
	return w.Close()
}
