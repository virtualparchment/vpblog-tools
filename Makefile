cross: build 
	env GOOS=darwin GOARCH=amd64 go build -o bin/backup-darwin-amd64 backup/backup.go
	env GOOS=linux GOARCH=amd64 go build -o bin/backup-linux-amd64 backup/backup.go

smoke: build
	env BACKUPLOCATIONS=$(shell pwd)/test_files/config.production.json,$(shell pwd)/test_files/content/data ARCHIVE=test_files/backup.tar.xz B2BUCKET=virtualparchment-ghost-backup bin/backup
	tar -tvf test_files/backup.tar.xz

build:
	go build -o bin/backup backup/backup.go

clean: 
	rm -rf bin