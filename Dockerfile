FROM golang:1.13-alpine

WORKDIR /go/src/app
COPY backup/*.go /go/src/app/
COPY go.* /go/src/app/

RUN go build -v ./...

CMD ["./backup"]
