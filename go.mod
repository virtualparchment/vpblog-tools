module vpblog-tools/backup

go 1.13

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/kurin/blazer v0.5.3
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/pierrec/lz4 v2.4.1+incompatible // indirect
	github.com/ulikunitz/xz v0.5.7 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
)
